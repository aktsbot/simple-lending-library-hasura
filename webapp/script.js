// ---------------- REPLACE VARS -------------------
/*
eg: 
const HASURA_APP_API = "https://foobar.herokuapp.com/v1/graphql";
const HASURA_APP_ROLE = "hello_shahidhk";
*/
const HASURA_APP_API = "https://simple-lending-library.herokuapp.com/v1/graphql";
const HASURA_APP_ROLE = "guest";
// ------------------------------------------------

const form = document.querySelector("#lend-book");
const btn = document.querySelector("#donate");

const areVarsSet = () => {
  if (HASURA_APP_API !== '' && HASURA_APP_ROLE !== '') {
    return true;
  }
  return false;
}

const setButtonText = (text) => {
  btn.innerHTML = text;
}

const toggleButton = () => {
  if (btn.disabled) {
    btn.disabled = false;
  } else {
    btn.disabled = true;
  }
  return;
}

// takes a book name and author name
const makeBookWithAuthorMutation = (book, author) => {
  const payload = `[{name: "${book}", author: {data: {name: "${author}"}}}]`
  return `
  mutation insert_book_with_author {
    insert_books
      (objects: ${payload}) {
      returning {
        id
        name
        author {
          id
          name
        }
      }
    }
  }`;
}

const lendBook = (ev) => {
  ev.preventDefault();

  const author = form.elements["author-name"].value;
  const book = form.elements["book-name"].value;

  if (!author || !book) {
    return;
  }

  const bookWithAuthorMutation = makeBookWithAuthorMutation(book, author);
  const fetchOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "x-hasura-role": HASURA_APP_ROLE
    },
    body: JSON.stringify({
      query: bookWithAuthorMutation
    })
  };

  toggleButton();
  setButtonText(" . . . ");

  fetch(HASURA_APP_API, fetchOptions)
    .then(res => res.json())
    .then(data => {
      toggleButton();
      setButtonText("Success");
      setTimeout(() => {
        setButtonText("Donate");
      }, 1500);
      form.reset();
    })
};

form.addEventListener("submit", lendBook)

// check each time the page is refreshed 
// or when its loaded for the first time for vars set
if (!areVarsSet()) {
  alert('Config variables are not set. Please open the script.js file and set them!')
}
