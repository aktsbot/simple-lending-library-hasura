# Simple Lending Library

A very small app built on top of the [Hasura](https://hasura.io/) 
GraphQL [engine](https://github.com/hasura/graphql-engine/) using 
vanilla javascript and Vue.js.

## Features

- Give the app a book name plus an author name, it'll invoke
the Hasura GraphQL API to save it to a PostgreSQL database.

## Demo

- **Vanilla JS:** https://0x00.aktsbot.in/
- **Vue.JS:** https://0x01.aktsbot.in/

## Setup

### 1. Hasura

- Spin up a Hasura [instance](https://github.com/hasura/graphql-engine/#quickstart). *(Heroku preferred)*
- Once the instance is deployed, visit it via `https://<app-name>.herokuapp.com`. You'll be greeted by the Hasura console.
- To prevent open access to the Hasura console. Set an **Admin Secret**.
  Refer the [docs](https://docs.hasura.io/1.0/graphql/manual/deployment/securing-graphql-endpoint.html) to set an environment variable, called
  `HASURA_GRAPHQL_ADMIN_SECRET`.
- In the `Data` tab, we'll get to manage our database & tables. 
  We need to add 2 tables(`authors` and `books`).
  - `authors`
    ```
    | id  | name          | 
    |-----|---------------|
    | 10  | J K Rowling   |
    ```
  - `books`
    ```
    | id | name          | author_id |
    |----|---------------|-----------|
    |  1 | Harry Potter  | 10        |
    ```
- For `authors`, make `id` the primary key + autoincrementing.
- For `books`, make `id` the primary key + autoincrementing. Also make
`author_id`, a foreign key to the `authors` table.
- It's a bummer to send two requests per addition of a book. So, by using
Hasura [relationships](https://docs.hasura.io/1.0/graphql/manual/mutations/insert.html#nested-inserts) 
this becomes a tad bit more easier.
```
mutation insert_book_with_author {
  insert_books
    (objects: [
      {
        name: "GraphQL Guide",
        author: {
          data: {
            name: "John"
          }
        }
      }
    ]
  ) {
    affected_rows
    returning {
      id
      name
      author {
        id
        name
      }
    }
  }
}
```

- We have an Object relationship `author` on `books` table, where `books.author_id -> authors.id` 


### 2. Webapp

- The app source is inside the [webapp](/webapp) folder.
- Open the `script.js` file and update `HASURA_APP_API` & `HASURA_APP_ROLE` variables. 
- Hasura requires authentication for requests. So, we create an 
[unauthenticated role](https://dev.to/mikewheaton/public-graphql-queries-with-hasura-2n06). Set the
role value to the `HASURA_APP_ROLE` variable.
- The value of `HASURA_APP_API` should be a link to the Hasura end-point
you've setup in the previous section.
- While in the `webapp` directory, run a 
  ```bash
  $ python -m SimpleHTTPServer 1337
  ```
  This would start a simple server on port `1337`
- Visit [http://localhost:1337/](http://localhost:1337/) to see the app.
- There is now a vue app inside the `vue-app` directory. See it's [README](vue-app/README.md)
for steps on how to spin it up.

## Resources
- https://docs.hasura.io/1.0/graphql/manual/mutations/multiple-mutations.html
- https://learn.hasura.io/graphql/vue
- https://cli.vuejs.org/guide/mode-and-env.html#environment-variables

## Roadmap / ToDo

The app just does an `INSERT` at this point. The following needs to 
be done, to complete the base `CRUD` ops.

[ ] Update the frontend to get a list of added books and authors.

[ ] Update the frontend for edit/update/delete operations on an author or a book. 

[x] Move to Vue + [Apollo](https://www.apollographql.com/) or any framework
 from vanilla js, if the UI + logic starts getting huge.
