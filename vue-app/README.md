# Simple Lending Library - Vue

The idea was to use a vanilla js implementation of Apollo, but that back-fired
when the vanilla js doc link, over at Apollo's docs point to their react tutorial.

## Setup

```
$ npm install
$ npm run serve # dev 
$ npm run build # builds for production
```

## Config

Replace the values in `.env.development` and `.env.production` files
with yours.

